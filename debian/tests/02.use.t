#!/bin/sh

test_description="functions"

. /usr/share/sharness/sharness.sh

fixtures=../fixtures

test_expect_success "functions" "
    puppet apply ${fixtures}/functions.pp
"

test_done
